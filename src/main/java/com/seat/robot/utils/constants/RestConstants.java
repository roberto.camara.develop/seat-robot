package com.seat.robot.utils.constants;

public class RestConstants {

	public static final String APPLICATION_NAME = "/seat-robot";
	public static final String API_VERSION_1 = "/v1";
	public static final String SUCCESS = "Success";

	public static final String RESOURCE_STATUS = "/status";
	public static final String RESOURCE_MOVEMENT = "/movement";
	public static final String RESOURCE_REPORT_HISTORY = "/report-history";
	public static final String RESOURCE_LOCATION = "/location";
	public static final String RESOURCE_MAPS = "/maps";
	public static final String RESOURCE_CURRENT_LOCATION = "/current-location";
	public static final String RESOURCE_COORDINATES = "/coordinates";
	public static final String RESOURCE_COORDINATES_POLYLINE = "/coordinates-polyline";
	public static final String RESOURCE_DATA = "/data";

	
	public static final String PARAMETER_CATEGORY = "categories";

	private RestConstants() {
		throw new IllegalStateException("Utility Class");
	}

}
