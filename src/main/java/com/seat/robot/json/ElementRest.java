package com.seat.robot.json;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ElementRest implements Serializable {

	private static final long serialVersionUID = 180802329613616000L;

	@JsonProperty("distance")
	private DistanceRest distanceRest;

	public DistanceRest getDistanceRest() {
		return distanceRest;
	}

	public void setDistanceRest(DistanceRest distanceRest) {
		this.distanceRest = distanceRest;
	}

}
