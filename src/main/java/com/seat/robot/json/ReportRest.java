package com.seat.robot.json;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ReportRest implements Serializable {

	private static final long serialVersionUID = 180802329613616000L;

	private Long timestamp;

	private Long id;

	private LocationRest location;

	private String level;

	private String source;

	public Long getTime() {
		return timestamp;
	}

	public void setTime(Long time) {
		this.timestamp = time;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocationRest getLocationRest() {
		return location;
	}

	public void setLocationRest(LocationRest locationRest) {
		this.location = locationRest;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	@Override
	public String toString() {
		return "ReportRest [time=" + timestamp + ", location=" + location + ", level=" + level
				+ ", source=" + source + "]";
	}

	
}
