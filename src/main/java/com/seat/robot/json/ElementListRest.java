package com.seat.robot.json;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ElementListRest implements Serializable {

	private static final long serialVersionUID = 180802329613616000L;

	@JsonProperty("elements")
	private List<ElementRest> elements;

	public List<ElementRest> getElements() {
		return elements;
	}

	public void setElements(List<ElementRest> elements) {
		this.elements = elements;
	}

}
