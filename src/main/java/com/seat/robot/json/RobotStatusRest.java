package com.seat.robot.json;

import java.io.Serializable;
import java.security.Timestamp;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class RobotStatusRest implements Serializable {

	private static final long serialVersionUID = 180802329613616000L;

	private Long id;

	private String origin;

	private String originLatLng;

	private String destination;

	private String destinationLatLng;

	private int metersFromOrigin;

	private int pm;

	private Timestamp time;

	private String source;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getOriginLatLng() {
		return originLatLng;
	}

	public void setOriginLatLng(String originLatLng) {
		this.originLatLng = originLatLng;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getDestinationLatLng() {
		return destinationLatLng;
	}

	public void setDestinationLatLng(String destinationLatLng) {
		this.destinationLatLng = destinationLatLng;
	}

	public int getMetersFromOrigin() {
		return metersFromOrigin;
	}

	public void setMetersFromOrigin(int metersFromOrigin) {
		this.metersFromOrigin = metersFromOrigin;
	}

	public int getPm() {
		return pm;
	}

	public void setPm(int pm) {
		this.pm = pm;
	}

	public Timestamp getTime() {
		return time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

}
