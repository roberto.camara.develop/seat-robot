package com.seat.robot.json;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DistanceRest implements Serializable {

	private static final long serialVersionUID = 180802329613616000L;
	
	@JsonProperty("value")
	private int meters;

	public int getMeters() {
		return meters;
	}

	public void setMeters(int meters) {
		this.meters = meters;
	}

}
