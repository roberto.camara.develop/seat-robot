package com.seat.robot.json;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DistanceGoogleResponseRest implements Serializable {

	private static final long serialVersionUID = 180802329613616000L;

	@JsonProperty("destination_addresses")
	private List<String> destinationAddresses;

	@JsonProperty("origin_addresses")
	private List<String> originAddresses;

	@JsonProperty("rows")
	private List<ElementListRest> rows;

	public List<String> getDestinationAddresses() {
		return destinationAddresses;
	}

	public List<String> getOriginAddresses() {
		return originAddresses;
	}

	public void setOriginAddresses(List<String> originAddresses) {
		this.originAddresses = originAddresses;
	}

	public List<ElementListRest> getRows() {
		return rows;
	}

	public void setRows(List<ElementListRest> rows) {
		this.rows = rows;
	}

	public void setDestinationAddresses(List<String> destinationAddresses) {
		this.destinationAddresses = destinationAddresses;
	}

}
