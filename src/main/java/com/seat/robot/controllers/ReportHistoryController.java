package com.seat.robot.controllers;

import java.util.List;

import com.seat.robot.exceptions.RobotException;
import com.seat.robot.json.ReportHistoryRest;
import com.seat.robot.response.RobotResponse;

public interface ReportHistoryController {

	RobotResponse<List<ReportHistoryRest>> getAllHistoryReports() throws RobotException;

}
