package com.seat.robot.controllers;

import java.util.List;

import com.seat.robot.exceptions.RobotException;
import com.seat.robot.json.DistanceGoogleResponseRest;
import com.seat.robot.json.LocationRest;
import com.seat.robot.json.PolylineRest;
import com.seat.robot.json.ReportRest;
import com.seat.robot.response.RobotResponse;

public interface RobotStatusController {

	RobotResponse<List<DistanceGoogleResponseRest>> getDistanceBetweenCoordinates(PolylineRest polylineRest)
			throws RobotException;

	RobotResponse<List<ReportRest>> getRobotData() throws RobotException;

	RobotResponse<List<LocationRest>> getCoordinatesFromPolyline(PolylineRest polylineRest) throws RobotException;

}
