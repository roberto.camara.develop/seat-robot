package com.seat.robot.controllers.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.seat.robot.json.LocationRest;
import com.seat.robot.services.ReportService;
import com.seat.robot.utils.constants.RestConstants;

import springfox.documentation.annotations.ApiIgnore;

@Controller
@ApiIgnore
@RequestMapping(RestConstants.APPLICATION_NAME + RestConstants.API_VERSION_1 + RestConstants.RESOURCE_MAPS)
public class MapLocationControllerImpl {

	@Autowired
	private ReportService reportService;

	@RequestMapping(value = RestConstants.RESOURCE_CURRENT_LOCATION, method = RequestMethod.GET)
	public ModelAndView showCurrentLocationOnMap() {

		final ModelAndView model = new ModelAndView("currentLocation");
		final LocationRest currentLocation = reportService.getCurrentLocation();
		model.addObject("latitude", currentLocation.getLat());
		model.addObject("longitude", currentLocation.getLng());
		model.addObject("coordinates", currentLocation.getLat() + "," + currentLocation.getLng());

		return model;
	}

}
