package com.seat.robot.controllers.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.seat.robot.controllers.ReportHistoryController;
import com.seat.robot.exceptions.RobotException;
import com.seat.robot.json.ReportHistoryRest;
import com.seat.robot.response.RobotResponse;
import com.seat.robot.services.ReportHistoryService;
import com.seat.robot.utils.constants.CommonConstants;
import com.seat.robot.utils.constants.RestConstants;

@RestController
@RequestMapping(RestConstants.APPLICATION_NAME + RestConstants.API_VERSION_1 + RestConstants.RESOURCE_REPORT_HISTORY)
public class ReportHistoryControllerImpl implements ReportHistoryController {

	@Autowired
	private ReportHistoryService reportHistoryService;

	@Override
	@ResponseStatus(HttpStatus.OK)
	@GetMapping(value = RestConstants.RESOURCE_DATA, produces = MediaType.APPLICATION_JSON_VALUE)
	public RobotResponse<List<ReportHistoryRest>> getAllHistoryReports() throws RobotException {
		return new RobotResponse<>(CommonConstants.SUCCESS, String.valueOf(HttpStatus.OK), CommonConstants.OK,
				reportHistoryService.getAllHistoryReports());
	}

}
