package com.seat.robot.controllers.impl;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.seat.robot.controllers.MovementController;
import com.seat.robot.exceptions.RobotException;
import com.seat.robot.json.PolylineRest;
import com.seat.robot.json.RobotRest;
import com.seat.robot.response.RobotResponse;
import com.seat.robot.services.MovementService;
import com.seat.robot.utils.constants.CommonConstants;
import com.seat.robot.utils.constants.RestConstants;

@RestController
@RequestMapping(RestConstants.APPLICATION_NAME + RestConstants.API_VERSION_1 + RestConstants.RESOURCE_MOVEMENT)
public class MovementControllerImpl implements MovementController {

	@Autowired
	private MovementService movementService;

	@Override
	@ResponseStatus(HttpStatus.OK)
	@PostMapping(value = "/start", produces = MediaType.APPLICATION_JSON_VALUE)
	public RobotResponse<RobotRest> startRobot(@RequestBody @Valid final PolylineRest polylineRest)
			throws RobotException {
		return new RobotResponse<>(CommonConstants.SUCCESS, String.valueOf(HttpStatus.OK), CommonConstants.OK,
				movementService.startRobot(polylineRest));
	}

	@Override
	@ResponseStatus(HttpStatus.OK)
	@DeleteMapping(value = "/stop", produces = MediaType.APPLICATION_JSON_VALUE)
	public RobotResponse<RobotRest> stopRobot(final String robotName) throws RobotException {
		return new RobotResponse<>(CommonConstants.SUCCESS, String.valueOf(HttpStatus.OK), CommonConstants.OK,
				movementService.stopRobot(robotName));
	}

}
