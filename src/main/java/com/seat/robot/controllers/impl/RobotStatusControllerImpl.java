package com.seat.robot.controllers.impl;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.seat.robot.controllers.RobotStatusController;
import com.seat.robot.exceptions.RobotException;
import com.seat.robot.json.DistanceGoogleResponseRest;
import com.seat.robot.json.LocationRest;
import com.seat.robot.json.PolylineRest;
import com.seat.robot.json.ReportRest;
import com.seat.robot.response.RobotResponse;
import com.seat.robot.services.ReportService;
import com.seat.robot.services.RobotStatusService;
import com.seat.robot.utils.constants.CommonConstants;
import com.seat.robot.utils.constants.RestConstants;

@RestController
@RequestMapping(RestConstants.APPLICATION_NAME + RestConstants.API_VERSION_1 + RestConstants.RESOURCE_STATUS)
public class RobotStatusControllerImpl implements RobotStatusController {

	@Autowired
	private RobotStatusService robotStatusService;

	@Autowired
	private ReportService reportService;

	@Override
	@ResponseStatus(HttpStatus.OK)
	@PostMapping(value = RestConstants.RESOURCE_COORDINATES, produces = MediaType.APPLICATION_JSON_VALUE)
	public RobotResponse<List<DistanceGoogleResponseRest>> getDistanceBetweenCoordinates(
			@RequestBody @Valid final PolylineRest polylineRest) throws RobotException {

		return new RobotResponse<>(CommonConstants.SUCCESS, String.valueOf(HttpStatus.OK), CommonConstants.OK,
				robotStatusService.getDistanceBetweenCoordinatesGoogle(polylineRest));
	}

	@Override
	@ResponseStatus(HttpStatus.OK)
	@GetMapping(value = RestConstants.RESOURCE_DATA, produces = MediaType.APPLICATION_JSON_VALUE)
	public RobotResponse<List<ReportRest>> getRobotData() throws RobotException {
		return new RobotResponse<>(CommonConstants.SUCCESS, String.valueOf(HttpStatus.OK), CommonConstants.OK,
				robotStatusService.getRobotData());
	}

	@Override
	@ResponseStatus(HttpStatus.OK)
	@PostMapping(value = RestConstants.RESOURCE_COORDINATES_POLYLINE, produces = MediaType.APPLICATION_JSON_VALUE)
	public RobotResponse<List<LocationRest>> getCoordinatesFromPolyline(
			@RequestBody @Valid final PolylineRest polylineRest) throws RobotException {
		return new RobotResponse<>(CommonConstants.SUCCESS, String.valueOf(HttpStatus.OK), CommonConstants.OK,
				robotStatusService.getCoordinatesfromPolyline(polylineRest));
	}

	@ResponseStatus(HttpStatus.OK)
	@GetMapping(value = RestConstants.RESOURCE_CURRENT_LOCATION, produces = MediaType.APPLICATION_JSON_VALUE)
	public RobotResponse<LocationRest> getCurrentLocation() throws RobotException {
		return new RobotResponse<>(CommonConstants.SUCCESS, String.valueOf(HttpStatus.OK), CommonConstants.OK,
				reportService.getCurrentLocation());
	}
}
