package com.seat.robot.controllers;

import com.seat.robot.exceptions.RobotException;
import com.seat.robot.json.PolylineRest;
import com.seat.robot.json.RobotRest;
import com.seat.robot.response.RobotResponse;

public interface MovementController {

	RobotResponse<RobotRest> startRobot(PolylineRest polylineRest) throws RobotException;

	RobotResponse<RobotRest> stopRobot(String robotName) throws RobotException;
}
