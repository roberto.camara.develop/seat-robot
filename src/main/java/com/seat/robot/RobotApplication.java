package com.seat.robot;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.seat.robot.exceptions.RobotException;
import com.seat.robot.json.PolylineRest;
import com.seat.robot.services.MovementService;
import com.seat.robot.services.impl.MovementServiceImpl;

@EnableFeignClients
@EnableScheduling
@SpringBootApplication
public class RobotApplication extends SpringBootServletInitializer {

	@Autowired
	private MovementService movementService;

	@Value("${initial.polyline}")
	private String polyline;

	private static final String DEFAULT_ROBOT = "DEFAULT_ROBOT";

	private static final Logger LOGGER = LoggerFactory.getLogger(MovementServiceImpl.class);

	public static void main(String[] args) {
		SpringApplication.run(RobotApplication.class, args);
	}

	@PostConstruct
	public void init() {
		final PolylineRest polylineRest = new PolylineRest();
		polylineRest.setPolyline(polyline);
		polylineRest.setRobotName(DEFAULT_ROBOT);
		try {
			movementService.startRobot(polylineRest);
		} catch (final RobotException e) {
			LOGGER.error(e.getMessage());
		}
	}
}
