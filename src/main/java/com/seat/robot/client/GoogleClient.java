package com.seat.robot.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.seat.robot.json.DistanceGoogleResponseRest;

@FeignClient(value = "google-api", url = "https://maps.googleapis.com/maps/api/distancematrix/")
public interface GoogleClient {
 
    @RequestMapping(method = RequestMethod.GET, value = "json?units=metrics&key=AIzaSyD69RwiLgJZmI0VeyxK9Si4iOPwZusS5Ac&origins={origins}&destinations={destinations}", produces = "application/json")
    DistanceGoogleResponseRest getDistanceGoogleResponse(@RequestParam(value="origins") String origin, @RequestParam(value="destinations") String destinations);
}

