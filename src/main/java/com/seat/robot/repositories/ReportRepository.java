package com.seat.robot.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.seat.robot.entities.Report;

@Repository
public interface ReportRepository extends JpaRepository<Report, Long> {

	List<Report> findBySource(String robotName);

}
