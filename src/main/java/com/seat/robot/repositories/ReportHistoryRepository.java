package com.seat.robot.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.seat.robot.entities.ReportHistory;

@Repository
public interface ReportHistoryRepository extends JpaRepository<ReportHistory, Long> {

}
