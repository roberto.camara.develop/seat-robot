package com.seat.robot.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.seat.robot.entities.RobotStatus;

@Repository
public interface RobotStatusRepository extends JpaRepository<RobotStatus, Long> {

}
