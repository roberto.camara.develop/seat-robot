package com.seat.robot.entities;

import java.security.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ROBOT_STATUS")
public class RobotStatus {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "ORIGIN")
	private String origin;

	@Column(name = "ORIGIN_LAT_LNG")
	private String originLatLng;

	@Column(name = "DESTINATION")
	private String destination;

	@Column(name = "DESTINATION_LAT_LNG")
	private String destinationLatLng;

	@Column(name = "METERS_FROM_ORIGIN")
	private int metersFromOrigin;

	@Column(name = "PM")
	private int pm;

	@Column(name = "TIME")
	private Timestamp time;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getOriginLatLng() {
		return originLatLng;
	}

	public void setOriginLatLng(String originLatLng) {
		this.originLatLng = originLatLng;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getDestinationLatLng() {
		return destinationLatLng;
	}

	public void setDestinationLatLng(String destinationLatLng) {
		this.destinationLatLng = destinationLatLng;
	}

	public int getMetersFromOrigin() {
		return metersFromOrigin;
	}

	public void setMetersFromOrigin(int metersFromOrigin) {
		this.metersFromOrigin = metersFromOrigin;
	}

	public int getPm() {
		return pm;
	}

	public void setPm(int pm) {
		this.pm = pm;
	}

	public Timestamp getTime() {
		return time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

}
