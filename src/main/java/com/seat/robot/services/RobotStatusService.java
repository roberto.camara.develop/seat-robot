package com.seat.robot.services;

import java.util.List;

import com.seat.robot.exceptions.RobotException;
import com.seat.robot.json.DistanceGoogleResponseRest;
import com.seat.robot.json.LocationRest;
import com.seat.robot.json.PolylineRest;
import com.seat.robot.json.ReportRest;

public interface RobotStatusService {

	List<DistanceGoogleResponseRest> getDistanceBetweenCoordinatesGoogle(PolylineRest polylineRest)
			throws RobotException;

	List<ReportRest> getRobotData() throws RobotException;

	List<LocationRest> getCoordinatesfromPolyline(PolylineRest polylineRest) throws RobotException;

}
