package com.seat.robot.services.impl;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.maps.model.LatLng;
import com.seat.robot.dto.ReportDto;
import com.seat.robot.entities.Report;
import com.seat.robot.exceptions.RobotException;
import com.seat.robot.json.LocationRest;
import com.seat.robot.json.ReportRest;
import com.seat.robot.repositories.ReportRepository;
import com.seat.robot.services.ReportHistoryService;
import com.seat.robot.services.ReportService;
import com.seat.robot.utils.enums.PollutionEnum;

@Service
public class ReportServiceImpl implements ReportService {

	@Autowired
	private ReportRepository reportRepository;

	@Autowired
	private ReportHistoryService reportHistoryService;

	private ModelMapper modelMapper = new ModelMapper();

	@Value("${initial.robot-name}")
	private String robotName;

	private LatLng buckinghamPalace = new LatLng(51.501299, -0.141935);

	private LatLng templeStation = new LatLng(51.510852, -0.114165);

	private static LatLng _currentPosition = new LatLng();

	private boolean isRobotStopped = false;

	private boolean isPalaceReportCreated = false;

	private boolean isTempleReportCreated = false;

	private int _currentAvgPollution;

	@Override
	public ReportRest createReport(LatLng origin, LatLng destination, double distance, int meters, String robotName)
			throws RobotException {
		final Report report = new Report();

		final LatLng currentPosition = calculateOffsetMeters(origin, destination, distance, meters);
		report.setLatitude(currentPosition.lat);
		report.setLongitude(currentPosition.lng);
		report.setPm(generateRandomInt());
		report.setSource(robotName);
		report.setTime(getCurrentTime().getTime());
		reportRepository.save(report);

		return createReportRest(reportRepository.save(report), robotName);
	}

	private ReportRest createReportRest(final Report report, final String robotName) {
		final ReportRest reportRest = new ReportRest();
		reportRest.setLevel(String.valueOf(report.getPm()));
		reportRest.setSource(robotName);
		reportRest.setTime(getCurrentTime().getTime());
		final LocationRest locationRest = new LocationRest();
		locationRest.setLat(report.getLatitude());
		locationRest.setLng(report.getLongitude());
		reportRest.setLocationRest(locationRest);
		return reportRest;
	}

	private Timestamp getCurrentTime() {
		return new Timestamp(System.currentTimeMillis());
	}

	private int generateRandomInt() {
		final int max = 200;
		final int min = 1;
		final int range = max - min + 1;
		return (int) (Math.random() * range) + min;
	}

	private LatLng calculateOffsetMeters(final LatLng origin, final LatLng destination, final double distance,
			final int meters) {
		final double result = meters / distance;
		_currentPosition.lat = origin.lat + ((destination.lat - origin.lat) * result);
		_currentPosition.lng = origin.lng + ((destination.lng - origin.lng) * result);

		return _currentPosition;
	}

	public double calculateDistanceByCoordinates(final LatLng origin, final LatLng destination) {
		int Radius = 6371;// radius of earth in Km
		double lat1 = origin.lat;
		double lat2 = destination.lat;
		double lon1 = origin.lng;
		double lon2 = destination.lng;
		double dLat = Math.toRadians(lat2 - lat1);
		double dLon = Math.toRadians(lon2 - lon1);
		double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(Math.toRadians(lat1))
				* Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
		double c = 2 * Math.asin(Math.sqrt(a));

		return Radius * c * 1000;
	}

	private int calculateAvgPollution(final List<Report> reportList) {
		if (!CollectionUtils.isEmpty(reportList)) {
			int sumPollution = 0;
			for (final Report report : reportList) {
				sumPollution += report.getPm();
			}
			_currentAvgPollution = sumPollution / reportList.size();
		}

		return _currentAvgPollution;
	}

	@Scheduled(fixedRate = 900000, initialDelay = 900000)
	public void printReport() {
		if (!isRobotStopped()) {
			final List<Report> reportList = reportRepository.findBySource(robotName);

			final ReportRest reportRest = new ReportRest();
			final LocationRest locationRest = new LocationRest();

			reportRest.setLevel(String.valueOf(PollutionEnum.getPollutionByInt(calculateAvgPollution(reportList))));
			reportRest.setSource(robotName);
			locationRest.setLat(_currentPosition == null ? 0 : _currentPosition.lat);
			locationRest.setLng(_currentPosition == null ? 0 : _currentPosition.lng);
			reportRest.setLocationRest(locationRest);
			reportRest.setTime(getCurrentTime().getTime());
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			System.out.println(gson.toJson(reportRest));

			if (!CollectionUtils.isEmpty(reportList)) {
				reportHistoryService.saveReports(reportList.stream()
						.map(report -> modelMapper.map(report, ReportDto.class)).collect(Collectors.toList()));

				reportRepository.deleteAll();
			}
		}
	}

	private void createReportToPrint(final LatLng currentPosition, final String sourceName, final int avgPollution) {
		final ReportRest reportRest = new ReportRest();
		final LocationRest locationRest = new LocationRest();

		reportRest.setLevel(String.valueOf(PollutionEnum.getPollutionByInt(avgPollution)));
		reportRest.setSource(sourceName);
		locationRest.setLat(_currentPosition == null ? 0 : _currentPosition.lat);
		locationRest.setLng(_currentPosition == null ? 0 : _currentPosition.lng);
		reportRest.setLocationRest(locationRest);
		reportRest.setTime(getCurrentTime().getTime());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		System.out.println(gson.toJson(reportRest));
	}

	public boolean isRobotStopped() {
		return isRobotStopped;
	}

	public void setRobotStopped(boolean isRobotStopped) {
		this.isRobotStopped = isRobotStopped;
	}

	@Override
	public void checkCurrentPositionNearPoints(LatLng origin, LatLng destination, double distance,
			int metersFromOrigin) {
		_currentPosition = calculateOffsetMeters(origin, destination, distance, metersFromOrigin);
		if (_currentPosition != null) {
			if (!isPalaceReportCreated) {
				final double result = calculateDistanceByCoordinates(_currentPosition, buckinghamPalace);
				if (result <= 100) {
					createReportToPrint(_currentPosition, "BuckingHam_Palace", _currentAvgPollution);
					isPalaceReportCreated = true;
				}
			}

			if (!isTempleReportCreated) {
				final double result = calculateDistanceByCoordinates(_currentPosition, templeStation);
				if (result <= 100) {
					createReportToPrint(_currentPosition, "Temple_Station", _currentAvgPollution);
					isTempleReportCreated = true;
				}
			}
		}

	}

	@Override
	public LocationRest getCurrentLocation() {
		final LocationRest currentLocationRest = new LocationRest();
		currentLocationRest.setLat(_currentPosition.lat);
		currentLocationRest.setLng(_currentPosition.lng);
		return currentLocationRest;
	}

}
