package com.seat.robot.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.maps.internal.PolylineEncoding;
import com.google.maps.model.LatLng;
import com.seat.robot.exceptions.RobotException;
import com.seat.robot.json.PolylineRest;
import com.seat.robot.json.RobotRest;
import com.seat.robot.services.MovementService;
import com.seat.robot.services.ReportService;

@Service
public class MovementServiceImpl implements MovementService {

	private static final Logger LOGGER = LoggerFactory.getLogger(MovementServiceImpl.class);

	@Autowired
	private ReportService reportService;

	private Map<String, Thread> threads = new HashMap<>();

	@Override
	public RobotRest startRobot(final PolylineRest polylineRest) throws RobotException {

		Thread thread = new Thread() {
			public void run() {

				int totalRobotDistance = 0;
				reportService.setRobotStopped(false);
				List<LatLng> list = PolylineEncoding.decode(polylineRest.getPolyline());

				for (int i = 0; i < list.size() - 1; i++) {
					final LatLng origin = list.get(i);
					final LatLng destination = list.get(i + 1);
					final double distance = reportService.calculateDistanceByCoordinates(origin, destination);
					int metersFromOrigin = 0;
					while (metersFromOrigin <= distance) {
						methodToTime();
						totalRobotDistance += 2;
						metersFromOrigin = metersFromOrigin + 2;
						reportService.checkCurrentPositionNearPoints(origin, destination, distance, metersFromOrigin);
						if (totalRobotDistance % 100 == 0) {
							try {
								reportService.createReport(origin, destination, distance, metersFromOrigin,
										polylineRest.getRobotName());
							} catch (final RobotException e) {
								LOGGER.error(e.getMessage());
							}
						}
					}

				}
				reportService.setRobotStopped(true);
			}

		};
		threads.put(polylineRest.getRobotName(), thread);
		thread.start();

		return createRobotRest(polylineRest.getRobotName());

	}

	private static void methodToTime() {
		try {
			TimeUnit.SECONDS.sleep(1);
		} catch (final InterruptedException e) {
			LOGGER.error(e.getMessage());
		}
	}

	private RobotRest createRobotRest(final String robotName) {
		final RobotRest robotRest = new RobotRest();
		robotRest.setName(robotName);
		return robotRest;
	}

	@Override
	public RobotRest stopRobot(final String robotName) throws RobotException {
		Thread threadToStop = threads.get(robotName);
		threadToStop.interrupt();
		threads.remove(robotName);

		return createRobotRest(robotName);
	}
}
