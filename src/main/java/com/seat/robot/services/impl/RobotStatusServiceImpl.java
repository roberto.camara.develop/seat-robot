package com.seat.robot.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.maps.internal.PolylineEncoding;
import com.google.maps.model.LatLng;
import com.seat.robot.client.GoogleClient;
import com.seat.robot.entities.Report;
import com.seat.robot.exceptions.RobotException;
import com.seat.robot.json.DistanceGoogleResponseRest;
import com.seat.robot.json.LocationRest;
import com.seat.robot.json.PolylineRest;
import com.seat.robot.json.ReportRest;
import com.seat.robot.repositories.ReportRepository;
import com.seat.robot.services.RobotStatusService;

@Service
public class RobotStatusServiceImpl implements RobotStatusService {

	@Autowired
	private GoogleClient googleClient;

	@Autowired
	private ReportRepository reportRepository;

	@Override
	public List<DistanceGoogleResponseRest> getDistanceBetweenCoordinatesGoogle(final PolylineRest polylineRest)
			throws RobotException {
		final List<LatLng> originsAndDestinations = decodePolyline(polylineRest.getPolyline());
		final List<DistanceGoogleResponseRest> googleResponseRest = new ArrayList<>();
		for (int i = 0; i < originsAndDestinations.size() - 1; i++) {
			DistanceGoogleResponseRest distanceGoogleResponseRest = getDistanceGoogleResponseRest(
					originsAndDestinations.get(i).toString(), originsAndDestinations.get(i + 1).toString());
			googleResponseRest.add(distanceGoogleResponseRest);
		}
		return googleResponseRest;
	}

	private List<LatLng> decodePolyline(final String polyline) {
		return PolylineEncoding.decode(polyline);
	}

	public List<ReportRest> getRobotData() throws RobotException {
		List<Report> reportList = reportRepository.findAll();
		final List<ReportRest> reportRestList = new ArrayList<>();
		for (final Report report : reportList) {
			final ReportRest reportRest = new ReportRest();
			final LocationRest locationRest = new LocationRest();
			locationRest.setLat(report.getLatitude());
			locationRest.setLng(report.getLongitude());
			reportRest.setLevel(String.valueOf(report.getPm()));
			reportRest.setSource(report.getSource());
			reportRest.setTime(report.getTime());
			reportRest.setLocationRest(locationRest);
			reportRestList.add(reportRest);
		}
		return reportRestList;
	}

	private DistanceGoogleResponseRest getDistanceGoogleResponseRest(final String origin, final String destination) {
		return googleClient.getDistanceGoogleResponse(origin, destination);
	}

	@Override
	public List<LocationRest> getCoordinatesfromPolyline(final PolylineRest polylineRest) throws RobotException {
		final List<LatLng> coordinates = decodePolyline(polylineRest.getPolyline());
		final List<LocationRest> coordinatesRest = new ArrayList<LocationRest>();
		for (final LatLng coordinate : coordinates) {
			final LocationRest locationRest = new LocationRest();
			locationRest.setLat(coordinate.lat);
			locationRest.setLng(coordinate.lng);
			coordinatesRest.add(locationRest);
		}

		return coordinatesRest;
	}
}
