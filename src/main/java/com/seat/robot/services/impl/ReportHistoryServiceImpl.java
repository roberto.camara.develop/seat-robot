package com.seat.robot.services.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.seat.robot.dto.ReportDto;
import com.seat.robot.entities.ReportHistory;
import com.seat.robot.json.ReportHistoryRest;
import com.seat.robot.repositories.ReportHistoryRepository;
import com.seat.robot.services.ReportHistoryService;

@Service
public class ReportHistoryServiceImpl implements ReportHistoryService {

	@Autowired
	private ReportHistoryRepository reportHistoryRepository;

	private ModelMapper modelMapper = new ModelMapper();

	@Override
	public void saveReports(final List<ReportDto> reportsDto) {

		reportHistoryRepository.saveAll(reportsDto.stream()
				.map(reportDto -> modelMapper.map(reportDto, ReportHistory.class)).collect(Collectors.toList()));

	}

	@Override
	public List<ReportHistoryRest> getAllHistoryReports() {
		return reportHistoryRepository.findAll().stream()
				.map(report -> modelMapper.map(report, ReportHistoryRest.class)).collect(Collectors.toList());
	}

}
