package com.seat.robot.services;

import com.google.maps.model.LatLng;
import com.seat.robot.exceptions.RobotException;
import com.seat.robot.json.LocationRest;
import com.seat.robot.json.ReportRest;

public interface ReportService {

	ReportRest createReport(LatLng origin, LatLng destination, double distance, int meters, String robotName)
			throws RobotException;

	double calculateDistanceByCoordinates(final LatLng origin, final LatLng destination);

	void setRobotStopped(boolean isRobotStopped);

	void checkCurrentPositionNearPoints(LatLng origin, LatLng destination, double distance, int metersFromOrigin);

	LocationRest getCurrentLocation();
}
