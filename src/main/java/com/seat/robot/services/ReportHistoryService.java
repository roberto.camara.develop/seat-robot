package com.seat.robot.services;

import java.util.List;

import com.seat.robot.dto.ReportDto;
import com.seat.robot.json.ReportHistoryRest;

public interface ReportHistoryService {

	void saveReports(List<ReportDto> reportsDto);

	List<ReportHistoryRest> getAllHistoryReports();
}
