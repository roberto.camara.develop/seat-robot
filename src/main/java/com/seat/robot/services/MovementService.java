package com.seat.robot.services;

import com.seat.robot.exceptions.RobotException;
import com.seat.robot.json.PolylineRest;
import com.seat.robot.json.RobotRest;

public interface MovementService {

	RobotRest startRobot(PolylineRest polylineRest) throws RobotException;

	RobotRest stopRobot(String robotName) throws RobotException;

}
